import { useEffect } from 'react'
import styles from '../styles/Frame.module.css'
import { usePlayer } from '../contexts/PlayerContext'
import Row from './Row'

const Frame = () => {
  const { currentIndex, currentGuess, handleInput, guesses, error, setError } =
    usePlayer() // this is using player

  useEffect(() => {
    if (error) {
      setTimeout(() => setError(''), 2000)
    }
  }, [error])

  useEffect(() => {
    window.addEventListener('keyup', handleInput)
    return () => window.removeEventListener('keyup', handleInput)
  }, [guesses, currentGuess, handleInput])

  return (
    <div className={styles.container}>
      <div className={styles.errorPopup} data-error={error !== ''}>
        {error}
      </div>
      {guesses.map((guess, index) => (
        <Row
          key={index}
          guess={currentIndex === index ? currentGuess : guess ?? ''}
          isSubmitted={currentIndex !== index && guess !== null}
          index={index}
        />
      ))}
    </div>
  )
}

export default Frame
