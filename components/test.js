fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(async (response) => {
    try {
      if (response.status === 200) {
        const data = await response.json();
        console.log(data);
      } else {
        const data = await response.text();
        console.log(data);
      }
    } catch (err) {
      console.error("Cannont parse the body to json");
    }
  })
  .catch(console.error);
